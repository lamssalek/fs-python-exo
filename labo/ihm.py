from laboratoire import *

def afficher_menu():
    print("0: Quitter ")
    print("1: Réserver un bureau")
    print("2: Enregistrer un départ")
    print("3: Modifier une reservation")
    print("4: Modifier membre du personnel")
    print("5: Membre du personnel")
    print("6: Statut du bureau")
    print("7: Listing membre du personnel")

def gerer(labo):
    nom = input("Nom ? ")
    bureau = input("Bureau ? ")
    try:
        enregistrer_arrivee(labo, nom, bureau)
    except PresentException:
        print(f"{nom} est déjà enregistrer dans ce bureau {bureau}")
    sauvegardejson(labo)

def gerer_depart(labo):
    nom = input("Nom ? ")
    try:
        enregistrer_depart(labo, nom)
    except AbsentException:
        print(f"{nom} n'a pas été enregistrer dans le labo")
    sauvegardejson(labo)

def gerer_modification(labo):
    nom = input("Nom à réaffecter: ")
    bureau = input(f"Nouveau bureau pour {nom}: ")
    try:
        modifier_bureau(labo, nom, bureau)
    except AbsentException:
        print(f"{nom} n'a pas été enregistrer dans le labo")
    sauvegardejson(labo)

def gerer_nom_personnel(labo):
    nom = input("Nom à modifier: ")
    newnom = input("Nouveau nom: ")
    try:
        modifier_nom(labo, nom, newnom)
    except AbsentException:
        print(f"{nom} n'a pas été enregistrer dans le labo")
    sauvegardejson(labo)

def gerer_membre_personel(labo):
    nom = input("Nom pour verification: ")
    present = verifier_membre(labo, nom)
    if present:
        print(f"{nom} fait bien partit du Labo")
    else:
        print(f"{nom} ne fait pas partit du Labo")
    

def gerer_bureau(labo):
    bureau = input("Numéro du Bureau: ")
    reserver = statut_bureau(labo, bureau)
    if reserver:
        print(f"le bureau {bureau} est affécter à{[k  for (k, val) in labo.items() if val == bureau]}")
    else:
        print(f"Le bureau {bureau} est libre")

def gerer_listing(labo):
    for nom, bureau in labo.items():
        print(f"{bureau}: \n- {nom} \n")

def traiter(choix, labo):
    if choix == 1:
        gerer(labo)
    elif choix == 2:
        gerer_depart(labo)
    elif choix == 3:
        gerer_modification(labo)
    elif choix == 4:
        gerer_nom_personnel(labo)
    elif choix == 5:
        gerer_membre_personel(labo)
    elif choix == 6:
        gerer_bureau(labo)
    elif choix == 7:
        gerer_listing(labo)
    else:
        print("Choisissez un menu valide")
        return afficher_menu()

def choix_utilisateur():
    return int(input(" Votre choix ... "))

def main():
    
    labo = Laboratoire()
    choix = None
    chargerjson()

    while choix != 0:
        afficher_menu()
        choix = choix_utilisateur()
        traiter(choix, labo)

if __name__ == '__main__':
    main()